const udp = require('dgram');
const crypto = require('crypto');
const MyEmiter = require('events').EventEmitter;

class Torrent extends MyEmiter{
    constructor(infoHash, urlObj){
        super();
        this.hash = infoHash;
        this.url = urlObj;
        this.transactionId;
        this.connectionId;
        this.connected = false;
        this.onConnectSuccessMessageCallback;

        this.socket = udp.createSocket({
            type: 'udp4',
        });

        this.socket.on('error', e=>{
            console.log(e.message);
        });

        this.socket.on('listening', ()=>{
            // console.log('Start listening');
        });

        this.socket.on('close', ()=>{
            console.log('Socket closed');
        });

        this.socket.on('message', msg=>{
            if(msg.length >= 16 && msg.length < 20){
                this.parseConnectionResponse(msg);
                this.connected = true;
                this.onConnectSuccessMessageCallback();
                this.emit('connectionRecieved');
            }else if(msg.length >= 20){
                this.parseAnnounceResponse(msg);
                this.emit('announceRecieved');
            }
            console.log('message');
        });

        this.sayHello = () => {
            let buff = Buffer.alloc(16);

            buff.writeUInt32BE(0x417, 0);
            buff.writeUInt32BE(0x27101980, 4);
            buff.writeUInt32BE(0, 8);
            buff.writeUInt32BE(Math.floor(Math.random() * 4294967295), 12)

            return this.sendMessage(buff);
        };

        this.announce = ()=>{
            let buf = Buffer.alloc(98);

            this.connectionId.copy(buf, 0);
            buf.writeUInt32BE(1, 8);
            buf.writeUInt32BE(Math.floor(Math.random() * 2500000000), 12);
            this.hash.copy(buf, 16);
            crypto.randomBytes(20).copy(buf, 36);
            buf.writeUInt32BE(0, 56);
            buf.writeUInt32BE(0, 60);
            buf.writeUInt32BE(0xfffff, 64);
            buf.writeUInt32BE(0xfffff, 64);
            buf.writeUInt32BE(0, 72);
            buf.writeUInt32BE(0, 76);
            buf.writeUInt32BE(0, 80);
            buf.writeUInt32BE(0, 84);
            buf.writeUInt32BE(Math.floor(Math.random() * 2500000000), 88);
            buf.writeUInt32BE(10, 92);
            buf.writeUInt16BE(6881, 96);

            this.sendMessage(buf);
        }

        this.sendMessage = (msg)=>{
            return new Promise((resolve, reject) => {
                this.socket.send(msg, 0, msg.length, this.url.port, this.url.hostname, err=>{
                    if(err) console.log(err.message);
                    this.onConnectSuccessMessageCallback = resolve;
                })
            })
        };

        this.parseConnectionResponse = (msg)=>{
            let action = msg.readUInt32BE(0);
            this.transactionId = msg.readUInt32BE(4);
            this.connectionId = Buffer.from([msg.readUInt32BE(8),msg.readUInt32BE(12)]);
        }

        this.parseAnnounceResponse = (msg)=>{
            let action = msg.readUInt32BE(0);
            this.transactionId = msg.readUInt32BE(4);
            this.reannounceInterval = msg.readUInt32BE(8);
            let leechers = msg.readUInt32BE(12);
            let seeders = msg.readUInt32BE(16);

            let st = 20;
            let num = msg.length - st % 6;

            this.peers = [];
            while(num-- && st + 6 <= msg.length){
                this.peers.push({
                    'ip': msg.readUInt8(st) + '.' + msg.readUInt8(st+1) + '.' + msg.readUInt8(st+2) + '.' + msg.readUInt8(st+3),
                    'port': msg.readUInt16BE(st+4),
                })

                st += 6;
            }
        }
    }
}


module.exports = Torrent;