const MongoClient = require('mongodb').MongoClient;

class MongoConnectionPromised{
    constructor(url, options, stayOpenedSeconds = 20){
        this.url = url;
        this.connectOptions = options;
        this.stayOpenned = stayOpenedSeconds;

        this.connect = ()=>{
            return new Promise((resolve, reject)=>{
                MongoClient.connect(this.url, this.connectOptions, (err, client)=>{
                    if(!err){
                        this.client = client;
                        if(this.stayOpenned > 0){
                            this.autoCloseTimer = setTimeout(()=>{
                                this.client.close();
                            }, this.stayOpenned * 1000);
                        }
                    }
                    err ? reject(err) : resolve();
                });
            });
        };

        this.close = ()=>{
            return new Promise((resolve,reject)=>{
                if(this.autoCloseTimer)
                    clearTimeout(this.autoCloseTimer);
                this.client.close();
                resolve();
            });
        };

        this.getDb = (dbName, options)=>{
            return new Promise((resolve, reject)=>{
                this.db = this.client.db(dbName, options);
                if(this.db){
                    resolve();
                }else{
                    reject('Error getting DB');
                }
            });
        };

        this.getCollection = (name, options = {}) => {
            return new Promise((resolve, reject)=>{
                if(!this.db)
                    reject('No db');
                else
                    this.db.collection(name, options, (err, collection)=>{
                        err ? reject(err) : resolve(collection);
                    });
            });
        };
        return this;
    }
}

module.exports = MongoConnectionPromised;

//Tests

function tests(){
    const mongoConnection = new MongoConnectionPromised('mongodb://localhost:27017', {
        'useNewUrlParser': true,
    }, 5);
    mongoConnection.connect()
    .then(()=>{console.log('Success')})
    .then(()=>{
        mongoConnection.getDb('ssp');
    })
    .then(()=>{
        return mongoConnection.getCollection('ssp');
    })
    .then((collection)=>{
        console.log('Collection recieved', collection);
    })
    .then(()=>{
        mongoConnection.close();
    })
    .catch(err=>{console.log('Error: ', err.message)});
}

(process.env['DEBUG'] && parseInt(process.env['DEBUG']) === 1) ? tests() : true;