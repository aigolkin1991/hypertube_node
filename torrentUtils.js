const DHT = require('bittorrent-dht');

function dhtSearch(hash){
    let dht = new DHT();
    dht.listen(20000, ()=>{
        console.log('DHT listening');
    });
    dht.on('peer', (peer, infoHash, from)=>{
        console.log('found potential peer ' + peer.host + ':' + peer.port + ' through ' + from.address + ':' + from.port);
    })
    dht.lookup(hash);
}
