module.exports = {
    'server': {
        'port': 3000,
    },

    'socket_io_server':{
        'port': 2020
    },

    'peerServer': {
        'port': 6881,
    },

    'mysql': {
        'host': '127.0.0.1',
        'user': 'artem',
        'password': '1qazxsw23e',
        'database': 'hypertube',
    },

    'dir': {
        'download': `${process.env.PWD}/downloads`,
        'cache': `${process.env.PWD}/cache`,
        'pwd': process.env.PWD,
        'downloadInProgressPrefix': 'progress_',
    }
};