const EventEmiter = require('events').EventEmitter;
const fetchMetadata = require('bep9-metadata-dl');
const config = require('./config');
const fs = require('fs');
const DHT = require('bittorrent-dht')

class TorrentDownloader extends EventEmiter{
    constructor(plainMagnet, parsedMagnet){
        super();
        this.plainMagnet = plainMagnet;
        this.parsedMagnet = parsedMagnet;
        this.metaData;
        this.files = {
            'fileInfoArr': [],
            'pieces': {},
            'mappedPieces': [],
            'pieceSize': 0,
        };
        this.downloadDir = `${config.dir.download}/${parsedMagnet.infoHash}`;
        this.metaCache = `${config.dir.cache}/${parsedMagnet.infoHash}_meta.json`;
        this.peers = {};

        if(!fs.existsSync(this.downloadDir))
            fs.mkdirSync(this.downloadDir);
    }

    getMetaData(){
        return new Promise((resolve, reject) => {
            if(parseInt(process.env['DEV']) === 1 && fs.existsSync(this.metaCache)){
                resolve();
                return ;
            }

            const INFO_HASH = this.parsedMagnet.infoHash; 
 
            fetchMetadata(INFO_HASH, { maxConns: 50, fetchTimeout: 40000, socketTimeout: 5000 },
                (err, metadata) => {
                if (err) {
                    console.log(err);
                    reject(err);
                    return;
                }
                this.metaData = metadata;
                console.log("METADATA: ", metadata);
                resolve();
            });
        });
    }

    mapPieces(){
        let numPieces = this.files.pieces.length / 20;
        let offset = 0;
        
        if(!this.files.mappedPieces) this.files.mappedPieces = [];

        while(numPieces--){
            let newBuff = Buffer.allocUnsafe(20);
            this.files.pieces.copy(newBuff, 0, offset, offset + 20);
            this.files.mappedPieces.push({pieceHash: newBuff, chunks: null, finished: false});
            offset += 20;
        }

        console.log("BUF LEN & PIECE NUM: ", this.files.pieces.length, this.files.pieces.length / 20);
    }

    parseMetadata(){
        return new Promise((resolve, reject) => {

            if(parseInt(process.env['DEV']) === 1 && fs.existsSync(this.metaCache)){
                this.files = JSON.parse(fs.readFileSync(this.metaCache));
                this.files.pieces = Buffer.from(this.files.pieces.data);
                resolve();
                return ;
            }

            let metaFiles = this.metaData.info;

            if(!metaFiles)
                reject(new Error('No metadata found'));
            if(!Array.isArray(metaFiles.files)){
                this.files.fileInfoArr.push({
                    'fileName': metaFiles.name.toString('utf8'),
                    'size': metaFiles.length,
                });
                this.files.pieceSize = metaFiles['piece length'];
                this.files.pieces = metaFiles.pieces;
                this._createFile(metaFiles.name.toString('utf8'), metaFiles.length);
            }else{
                for(let v of metaFiles.files){
                    this.files.fileInfoArr.push({
                        'fileName': v.path.toString('utf8'),
                        'size': v.length,
                    });
                    this._createFile(v.path.toString('utf8'), v.length);
                }
                this.files.pieceSize = metaFiles['piece length'];
                this.files.pieces = metaFiles.pieces;
            }

            if(parseInt(process.env['DEV']) === 1 && !fs.existsSync(this.metaCache)){
                fs.writeFileSync(this.metaCache, JSON.stringify(this.files));
            }
            resolve();
        });
    }

    _createFile(filename, filesize){
        const file =`${this.downloadDir}/${filename}`;
        if(!fs.existsSync(file))
        fs.writeFileSync(file, Buffer.alloc(filesize, 0), {flag: 'a'});
    }

    searchPeers(){
        return new Promise((resolve, reject) => {
            console.log('Peers object: ', this.peers);
            var dht = new DHT()
 
            dht.listen(20000, function () {
              console.log('now listening')
            })
            dht.on('peer', (peer, infoHash, from) => {
              console.log('found potential peer ' + peer.host + ':' + peer.port + ' through ' + from.address + ':' + from.port)
              if(!this.peers[peer.host]){
                  this.peers[peer.host] = {
                      'host': peer.host,
                      'port': peer.port,
                      'busy': false,
                  };
              }
            })
            // find peers for the given torrent info hash
            dht.lookup(this.parsedMagnet.infoHash);
            setTimeout(()=>{
                console.log('Number of unique peers: ', Object.keys(this.peers).length);
                resolve();
            }, 8000);
        });
    }

    _downloadSinglePiece(pieceHash, downloadEnv){
        let infoHash = this.parsedMagnet.infoHash;
        
        for(let v of this.peers){

        }
    }

    _downloadPieceControll(downloadEnv){
        if(downloadEnv.pieceIndex >= downloadEnv.pieceNum){
            return ;
        }else{
            if(downloadEnv < 200){
                this._downloadSinglePiece(this.files.mappedPieces[downloadEnv.pieceIndex], downloadEnv);
                downloadEnv.pieceIndex++;
                downloadEnv.activeDownloads++;
                this._downloadPieceControll(downloadEnv);
            }else{
                setTimeout(() => {
                    this._downloadPieceControll(downloadEnv);
                }, 3000);
            }
        }
    }

    downloadPieces(){
        return new Promise((resolve, reject) => {
            let downloadEnv = {
                activeDownloads: 0,
                pieceIndex: 0,
                pieceNum: this.files.mappedPieces.length,
                pieceSize: this.files.pieceSize,
            };
    
            this._downloadPieceControll(downloadEnv);
            setInterval(() => {
                for(let v of this.mappedPieces){
                    if(v.finished === false) return;
                    resolve();
                }
            }, 6000);
        });
    }
}

module.exports = TorrentDownloader;