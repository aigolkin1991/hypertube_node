const TorrentDownloader = require('./torrent_downloader');

class Torrent {
    constructor(hash, plainMagnet, parsedMagnet, firstListener){
        this.plainMagnet = plainMagnet;
        this.parsedMagnet = parsedMagnet;
        this.hash = hash;
        this.responseListeners = [firstListener];
        this.fileSrcPath = '';
        this.torrentDownloader;
        this.eventMap = {
            sendSrcLink: (timeout) => {
                setTimeout(()=>{
                    /**
                    * TODO check play movie possibility
                    */
                    console.log('sendSrcLink fired')
                    if(this.isPlayable()){
                        for(let listener of this.responseListeners){
                            if(listener.connected){
                                listener.send(JSON.stringify({'client_action': 'download', 'status': true, 'data': this.fileSrcPath}));
                                console.log('Send src link to active listeners')
                            }
                        }
                        this.responseListeners = [];
                    }else{
                        this.chargeEvenet('sendSrcLink', 16000);
                    }
                }, timeout);
            },
            removeFromDownloadManager: () => {
                setTimeout(()=>{
                    global.downloadsManager.removeItem(this.hash);
                }, 1000);
            } 
        };
    }

    chargeEvenet(eventName, timeout = 2000) {
        this.eventMap[eventName](timeout);
    }

    isPlayable(){
        /**
        * TODO check play movie possibility
        */
        if(this.fileSrcPath.length > 0){
            return true;
        }
    }

    startDownloading(){
        this.torrentDownloader = new TorrentDownloader(this.plainMagnet, this.parsedMagnet);
        this.torrentDownloader.getMetaData()
        .then(() => {
            console.log('Metadata recieved, next operation >>>');
            return this.torrentDownloader.parseMetadata();
        })
        .then(() => {
            console.log('Metadata parsed, next operation >>>');
            return this.torrentDownloader.mapPieces();
        })
        .then( () => {
            console.log('Pieces mapped >>>');
            return this.torrentDownloader.searchPeers();
        })
        .then( () => {
            return this.torrentDownloader.downloadPieces();
        })
        .catch((e) => {
            throw e;
        });
    }
}

module.exports = Torrent;