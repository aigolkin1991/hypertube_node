const config = require('./config');
const parseTorrent = require('parse-torrent');
const io = require('socket.io')(config.socket_io_server.port);
const redis = require('redis');
const redisClient = redis.createClient();
const Torrent = require('./Torrent_proto');
const fs = require('fs');

if(!fs.existsSync(config.dir.download))
    fs.mkdirSync(config.dir.download, 0744);

if(!fs.existsSync(config.dir.cache))
    fs.mkdirSync(config.dir.cache, 0744);

const dirContents = fs.readdirSync(config.dir.download, {withFileTypes: true});
for(let v of dirContents){
    //TODO improve unocmpleted downloads removal
    /**
     * way 1) Check dir names in redis as keys, in no value - remove dir as incomplete
     */
    if(v.name.indexOf(config.dir.downloadInProgressPrefix) !== -1){
        fs.rmdirSync(`${config.dir.download}/${v.name}`);
    }else if(!v.isDirectory()){
        fs.unlinkSync(`${config.dir.download}/${v.name}`);
    }
}

console.log(dirContents);


redisClient.on('connect', () => {
    console.log('Redis connection esteblished');
});

redisClient.on('error', (err) => {
    console.log('Something went wrong ' + err);
    process.exit();
});

global.downloadsManager = {
    downloads: {},
    addItem: (hash, torrent, listener) => {
        if(!global.downloadsManager.downloads[hash]){
            global.downloadsManager.downloads[hash] = torrent;
            global.downloadsManager.downloads[hash].responseListeners.push(listener);
            global.downloadsManager.downloads[hash].chargeEvenet('sendSrcLink');
            console.log('Add new item');
            torrent.startDownloading();
            return true;
        }else{
            return false;
        }
    },
    removeItem: (hash) => {
        if(global.downloadsManager.downloads[hash]){
            delete global.downloadsManager.downloads[hash];
        }
    },
    isInProgress: (hash) => {
        if(global.downloadsManager.downloads[hash]){
            console.log('Download in progress');
            return true;
        }else{
            return false;
        }
    },
    isDownloaded: (hash) => {
        return new Promise((resolve, reject) => {
            redisClient.get(hash, (e, r) => {
                if(e) reject(e);
                resolve(r);
                console.log('Redis search result', r)
            });
        });
    },
    adListenerToActiveDownload: (hash, listener) => {
        console.log('Add to active');
        if(global.downloadsManager.downloads[hash]){
            global.downloadsManager.downloads[hash].responseListeners.push(listener);
            global.downloadsManager.downloads[hash].chargeEvenet('sendSrcLink');
        }else{
            setTimeout(() => {
                global.downloadsManager.adListenerToActiveDownload(hash, listener);
            }, 1000);
        }
    }
};

const actionList = ['download'];

const actionHandlerMap = {
    'download': getMovie, 
};

function parseMessage(msg){
    try{
        const request = JSON.parse(msg);
        const action = Object.keys(request)[0];

        if(action && actionList.indexOf(action) !== -1){
            return {
                'action': action,
                'action_args': [request[action]],
            }
        }else{
            return false;
        }
    }catch(e){
        throw(e);
    }
}

function runAction(action, actionArgs, responseListener){
    try{
        actionHandlerMap[action](actionArgs, responseListener);
    }catch(e){
        throw e;
    }
}

function getMovie(magnetLinksArr, responseListener){
    const parsedTorrent = parseTorrent(magnetLinksArr[0]);
    const infoHash = parseInt(process.env['DEV']) === 1 ? 'dev_' + parsedTorrent.infoHash : parsedTorrent.infoHash;

    if(global.downloadsManager.isInProgress(infoHash)){
        global.downloadsManager.adListenerToActiveDownload(infoHash, responseListener);
    }else{
        global.downloadsManager.isDownloaded(infoHash)
        .then(searchResult => {
            if(searchResult !== null){
                responseListener.send(JSON.stringify({'client_action': 'download', 'status': true, 'data': searchResult}))
            }else{
                const res = global.downloadsManager.addItem(infoHash, new Torrent(infoHash, magnetLinksArr[0], parsedTorrent, responseListener));
                if(res === false)
                    global.downloadsManager.adListenerToActiveDownload(infoHash, responseListener);
            }
        })
        .catch(e => {
            throw e;
        });
    }
}

io.on('connection', (sock) => {
    sock.send(JSON.stringify({'client_action': 'connect', 'status': true}));
    sock.on('message', (m) => {
        console.log("Message recieved: ", m);

        try{
            const requestedAction = parseMessage(m);
            if(!requestedAction) sock.send(JSON.stringify({'client_action': 'unknow', 'status': false, 'description': 'undefined action'}));
            else {
                try{
                    runAction(requestedAction['action'], requestedAction['action_args'], sock)
                }catch(e){
                    sock.send(JSON.stringify({'client_action': requestedAction['action'], 'status': false, 'description': e.message}))
                }
            }
        }catch(e){
            sock.send(JSON.stringify({'client_action': 'unknow', 'status': false, 'description': e.message}));
        }
    });
    sock.on('disconnect', () => {
        console.log('Server socket closed');
        sock.disconnect();
    });
});