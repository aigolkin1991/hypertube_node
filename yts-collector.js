const http = require('https');
const MongoConnection = require('./MongoDBPromised');

const mongo = new MongoConnection('mongodb://localhost:27017', {
    'useNewUrlParser': true,
    'user': 'hypertube',
    'password': '1234',
}, 0);

mongo.connect()
.then(()=>{
    mongo.getDb('hypertube');
    return mongo.getCollection('movies');
})
.then(collection=>{
    getYtsData(1, collection);
})
.then(data=>{
    console.log(data);
})
.catch(e=>{
    console.log(e);
});

function getYtsData(page, collection){
    return new Promise((resolve, reject) => {
        let HTTPrequest = http.request(`https://yts.am/api/v2/list_movies.json?page=${page}&limit=50`, (response) => {
            let responseBody = "";
            response.setEncoding('utf8');
            response.on('data', (chunk) => {
                responseBody += chunk;
            });
            response.on('end', () => {
                let data;
                let count = 0;
                let movies = [];
                try {
                    data = JSON.parse(responseBody);
                    if(data.status === 'ok' && data.data.movie_count){
                        count = data.data.movie_count;
                    }
                    if(data.data.movies){
                        movies = data.data.movies;
                    }
                } catch (err) {
                    data = {};
                }
                writeMovies(movies, collection)
                .then(()=>{console.log('Movies writed')}, (e)=>{console.log(e)});
                if(movies.length > 0 && page * 50 < count){
                    return getYtsData(page+1, collection);
                }else{
                    resolve();
                }
            });
        });
        HTTPrequest.on('timeout', () => {
            HTTPrequest.destroy();
            reject(`Timeout in connect nodejs ${ip}`);
        });
        HTTPrequest.on('error', (err) => {
            HTTPrequest.destroy();
            reject(`Error in connect nodejs ${ip}`);
        });
        HTTPrequest.end();
    });
}

function writeMovies(movies, collection){
    return new Promise((resolve, reject)=>{
        for(let v of movies){
            let magnets_arr = [];
            if(v.torrents && v.torrents.length > 0){
                for(let t of v.torrents){
                    magnets_arr.push(`magnet:?xt=urn:btih:${t.hash}&dn=${encodeURIComponent(v.title_long)}&tr=udp://open.demonii.com:1337/announce&tr=udp://tracker.openbittorrent.com:80&tr=udp://tracker.coppersurfer.tk:6969&tr=udp://glotorrents.pw:6969/announce&tr=udp://tracker.opentrackr.org:1337/announce&tr=udp://torrent.gresille.org:80/announce&tr=udp://p4p.arenabg.com:1337&tr=udp://tracker.leechers-paradise.org:6969`);
                }
            }
            let tmp = {
                imdb: v.imdb_code || '',
                title: v.title_long,
                year: v.year,
                genres: v.genres,
                description: v.description_full,
                language: v.language,
                bimg: v.background_image || '',
                smimg: v.small_cover_image || '',
                lgimg: v.large_cover_image || '',
                magnets: magnets_arr,
            }
            collection.insertOne(tmp, {
                forceServerObjectId: true
            }, (e)=>{
                if(e){
                    console.log(e.message);
                }
            });
        }
        resolve();
    });
}