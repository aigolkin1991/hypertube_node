const mysql = require('mysql');

class MysqlConnectionPromised{
    constructor(mysqlConfig, stayConnectedSeconds = 20, createPool = false){
        this.mysqlConfig = mysqlConfig;
        this.stayConnectedSeconds = stayConnectedSeconds;
        this.pool = createPool;

        this.createConnection = ()=>{
            if(this.pool){
                if(!this.mysqlConfig.connectionLimit){
                    this.mysqlConfig.connectionLimit = 10;
                }
                this.connection = mysql.createPool(this.mysqlConfig);
            }else{
                console.log('Simple connection created');
                this.connection = mysql.createConnection(this.mysqlConfig);
                this.connection.on('error', (err)=>{
                    console.log(err.code);
                });
            }
            
            // return this;
        };
        this.connect = ()=>{
            return new Promise((resolve, reject)=>{
                this.connection.connect((err) => {
                    if(err){
                        reject(err);
                    }else if(stayConnectedSeconds > 0){
                        this.delayedConnectionEnd = setTimeout(()=>{
                            this.connection.end();
                        }, 1000 * stayConnectedSeconds);
                        resolve();
                    }
                });
            });
        };
        this.end = ()=>{
            return new Promise((resolve, reject)=>{
                if(this.delayedConnectionEnd){
                    clearTimeout(this.delayedConnectionEnd);
                }
                this.connection.end(err=>{
                    err ? reject(err) : resolve();
                });
            });
        };
        this.queryWithClose = (query, closeOnResult = false)=>{
            return new Promise((resolve, reject) => {
                this.connection.query(query, (err, result)=>{
                    if(closeOnResult){
                        if(this.delayedConnectionEnd){
                            clearTimeout(this.delayedConnectionEnd);
                        }
                        if(this.pool){
                            this.connection.on('release', ()=>{
                                this.connection.end(conErr=>{
                                    if(conErr){
                                        reject(conErr);
                                    }
                                });
                            });
                        }else{
                            this.connection.end(conErr=>{
                                if(conErr){
                                    resolve(conErr);
                                }
                            })
                        }
                    }
                    if(err){
                        reject(err)
                    }else{
                        resolve(result)
                    }
                });
            });
        };
    }
}

module.exports = MysqlConnectionPromised;